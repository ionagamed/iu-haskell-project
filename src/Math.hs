module Math where

-- | A simple 2-vector of floats.
type V2 = (Float, Float)

-- | Add two vectors.
vAdd :: V2 -> V2 -> V2
vAdd (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

-- | Subtract to vectors.
vSub :: V2 -> V2 -> V2
vSub (x1, y1) (x2, y2) = (x1 - x2, y1 - y2)

-- | Multiply a vector by a scalar value.
vMul :: V2 -> Float -> V2
vMul (x, y) a = (x * a, y * a)

-- | Negate the vector.
vNeg :: V2 -> V2
vNeg (x, y) = (- x, - y)

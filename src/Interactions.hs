{-# LANGUAGE ExistentialQuantification #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}

module Interactions where

import Control.Lens hiding (uncons)
import Data.Chain
import Data.List
import Data.Resources
import Framework.Types
import Types.Character
import Types.Enemies.BlueSlime
import Types.Enemies.Chaser
import Types.FloorTile
import Types.Items
import Types.Mob
import Types.Player
import Types.SmoothPosition
import Types.WorldMap

updateInteractions :: Map -> ActionCollector Map
updateInteractions map =
  updateAllMobInteractions map >>= updatePlayerInteractions

updatePlayerInteractions :: Map -> ActionCollector Map
updatePlayerInteractions currentMap =
  return $
    over players (map (updatePlayerCollisionInteractions currentMap)) currentMap

updatePlayerCollisionInteractions :: Map -> Player -> Player
updatePlayerCollisionInteractions currentMap currentPlayer =
  if shouldSwitch then newPlayer else currentPlayer
  where
    newPlayer =
      set
        (character . position)
        (swapSmoothPosition (currentPlayer ^. character . position))
        currentPlayer
    currentPlayerPosition = currentPlayer ^. character . position
    -- TODO: this code is really full of magic numbers
    shouldSwitch = colliding && (currentPlayer ^. character . position . time < 0.95)
    colliding =
      not
        ( any positionSameAsPlayer
            $ map floorTilePosition
            $ currentMap ^. floorTiles
        )
        || not (null collidingMobs)
    positionSameAsPlayer = (== currentPlayerPosition ^. current)
    collidingMobs =
      filter
        positionSameAsPlayer
        ( map (mobGridPosition . (^. Types.Enemies.BlueSlime.mob)) (currentMap ^. blueSlimes)
            <> map (mobGridPosition . (^. Types.Enemies.Chaser.mob)) (currentMap ^. chasers)
        )

handlePlayerMobCollision :: ((Float, Float) -> Bool) -> Lens' a Mob -> a -> a
handlePlayerMobCollision collisionF innerMob mob
  | collisionF (mobGridPosition $ mob ^. innerMob) =
    set
      innerMob
      (killMob $ mob ^. innerMob)
      mob
  | otherwise = mob

updateAllMobInteractions :: Map -> ActionCollector Map
updateAllMobInteractions me =
  chainM (map (\p -> updateMobInteractions (p ^. clientId)) (me ^. players)) me

updateMobInteractions :: ClientId -> Map -> ActionCollector Map
updateMobInteractions clientId' me = do
  let maybeCurrentPlayer = findPlayerWithId clientId' me
  case maybeCurrentPlayer of
    Nothing -> return me
    Just currentPlayer -> do
      (newBlueSlimes, playerSlimesCollisions) <-
        unzip <$> mapM (updateMobInteraction currentPlayer me Types.Enemies.BlueSlime.mob) (me ^. blueSlimes)
      (newChasers, playerChasersCollisions) <-
        unzip <$> mapM (updateMobInteraction currentPlayer me Types.Enemies.Chaser.mob) (me ^. chasers)
      newDamageAnimationTime <-
        if or (playerSlimesCollisions <> playerChasersCollisions)
           then do
             perform (PlaySound hurt1Sound)
             return 0.25
           else return $ currentPlayer ^. character . damageAnimationTime
      let newPlayer =
            set
              (character . damageAnimationTime)
              newDamageAnimationTime
              currentPlayer
      let newMe = replacePlayerWithId clientId' me newPlayer
      return
        $ set blueSlimes newBlueSlimes
        $ set
          chasers
          newChasers
          newMe

updateMobInteraction :: Player -> Map -> Lens' a Mob -> a -> ActionCollector (a, Bool)
updateMobInteraction currentPlayer map myInnerMob me = return (newMe, playerCollision)
  where
    newMe =
      set
        (myInnerMob . character . position)
        ( if playerCollision
            then swapSmoothPosition $ me ^. myInnerMob . character . position
            else me ^. myInnerMob . character . position
        )
      $ set
        (myInnerMob . shouldDie)
        (kill || (me ^. myInnerMob . shouldDie))
      me
    currentPlayerPosition = currentPlayer ^. character . position
    playerCollision = colliding && currentPlayerPosition ^. time < 0.95
    kill = colliding && currentPlayerPosition ^. time >= 0.95
    currentPlayerGridPosition = playerGridPosition currentPlayer
    myGridPosition = mobGridPosition $ me ^. myInnerMob
    colliding = currentPlayerGridPosition == myGridPosition

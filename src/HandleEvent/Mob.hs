module HandleEvent.Mob where

import Control.Lens
import Data.Fixed (mod')
import Framework.Types
import Math
import Types.Character
import Types.Direction
import Types.Enemies.BlueSlime
import Types.Enemies.Chaser
import Types.Mob
import Types.Player
import Types.SmoothPosition

mobDirectionVector :: Mob -> V2
mobDirectionVector me = directionToVector $ me ^. direction

moveMob :: Float -> Mob -> Mob
moveMob localDt me =
  set
    character
    (moveCharacter localDt 2 (mobDirectionVector me) (me ^. character))
    me

handleEventBlueSlime :: Event -> BlueSlime -> ActionCollector (Maybe BlueSlime)
handleEventBlueSlime (Update dt localDt) me
  | localDt > 0.2 && me ^. Types.Enemies.BlueSlime.mob . shouldDie = return Nothing
  | otherwise =
    return $ Just $ moveBlueSlime localDt $
      set
        (Types.Enemies.BlueSlime.mob . character . position)
        (updateSmoothPosition (me ^. Types.Enemies.BlueSlime.mob . character . position) dt)
        me
handleEventBlueSlime _ me = return (Just me)

moveBlueSlime :: Float -> BlueSlime -> BlueSlime
moveBlueSlime localDt me =
  set
    (Types.Enemies.BlueSlime.mob . direction)
    newDirection
    $ set
      Types.Enemies.BlueSlime.mob
      (moveMob localDt $ me ^. Types.Enemies.BlueSlime.mob)
      me
  where
    dt = localDt `mod'` 0.25 * 4
    newDirection =
      if (me ^. Types.Enemies.BlueSlime.mob . character . waitBeats == 0)
        && not (me ^. Types.Enemies.BlueSlime.mob . character . handledThisLoop && dt > 0.5)
        then oppositeDirection $ me ^. Types.Enemies.BlueSlime.mob . direction
        else me ^. Types.Enemies.BlueSlime.mob . direction

handleEventChaser :: Event -> [Player] -> Chaser -> ActionCollector (Maybe Chaser)
handleEventChaser (Update dt localDt) players me
  | localDt > 0.2 && me ^. Types.Enemies.Chaser.mob . shouldDie = return Nothing
  | otherwise =
    return $ Just $ moveChaser localDt
      $ set
        (Types.Enemies.Chaser.mob . character . position)
        (updateSmoothPosition (me ^. Types.Enemies.Chaser.mob . character . position) dt)
      $ set
        (Types.Enemies.Chaser.mob . direction)
        (if not (null players)
            then
              estimateDirection $
                vSub
                  (playerGridPosition closestPlayer)
                  (mobGridPosition $ me ^. Types.Enemies.Chaser.mob)
                      
            else
              None
        ) 
      me 
  where
    closestPlayer = foldr1 (getClosestTo me) players
handleEventChaser _ _ me = return (Just me)

getClosestTo :: Chaser -> Player -> Player -> Player
getClosestTo me p1 p2
  | d1 > d2   = p2
  | otherwise = p1
  where
    d1 = distance 
          (p1 ^. character . position . current) 
          (me ^. Types.Enemies.Chaser.mob . character . position . current)
    d2 = distance 
          (p2 ^. character . position . current) 
          (me ^. Types.Enemies.Chaser.mob . character . position . current)
   

moveChaser :: Float -> Chaser -> Chaser
moveChaser localDt me =
  set
    Types.Enemies.Chaser.mob
    (moveMob localDt $ me ^. Types.Enemies.Chaser.mob)
    me

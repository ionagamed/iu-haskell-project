module HandleEvent.Player where

import Control.Lens
import Data.Fixed
import Framework.Types
import Math
import Types.Character
import Types.Player
import Types.SmoothPosition

-- | Handle a player's event.
handleEventPlayer :: Event -> Player -> ActionCollector Player
handleEventPlayer (KeyPressOf clientId' key) me =
  if clientId' == me ^. clientId
    then handleKeyPressPlayer key me
    else return me
handleEventPlayer e me = handleUpdatePlayer e me

-- | Handle a key press for a player.
handleKeyPressPlayer :: KeyPress -> Player -> ActionCollector Player
handleKeyPressPlayer key me =
  return $
    if me ^. canMove
      then movePlayer key me
      else set (character . damageAnimationTime) 0.2 me

-- | Handle an update in game time for a player.
handleUpdatePlayer :: Event -> Player -> ActionCollector Player
handleUpdatePlayer (Update dt localDt) me =
  return
    $ updatePlayerAnimationTime dt
    $ updatePlayerMovementLimitations
      localDt
      me
handleUpdatePlayer _ me = return me

-- | Update player's animation times -- the character animation time and the
-- damage animation time.
updatePlayerAnimationTime :: Float -> Player -> Player
updatePlayerAnimationTime dt me =
  set (character . position) (updateSmoothPosition (me ^. character . position) dt) $
    set
      (character . damageAnimationTime)
      (max 0 (me ^. character . damageAnimationTime - dt))
      me

-- | Update the player with respect to movement limitations, such as: out of
-- bounds on the map, getting hit by an NPC, etc.
updatePlayerMovementLimitations :: Float -> Player -> Player
updatePlayerMovementLimitations localDt me = newMe
  where
    playerMoved = not $ characterCanMove $ me ^. character
    newMe
      | playerCanMoveAt localDt && not playerMoved =
        set canMove True me
      | not (playerCanMoveAt localDt) && playerMoved =
        set (character . waitBeats) 0 me
      | otherwise = me

-- | Whether the player can move at this local time moment.
playerCanMoveAt :: Float -> Bool
playerCanMoveAt localDt = abs dt < leeway
  where
    fractions = 0.25
    leeway = 0.05
    dt = localDt `mod'` fractions - fractions / 2

-- | Convert a `KeyPress` into a 2-vector describing the direction which is
-- being pressed.
playerKeyVector :: KeyPress -> V2
playerKeyVector KeyUp = (0, -1)
playerKeyVector KeyDown = (0, 1)
playerKeyVector KeyLeft = (-1, 0)
playerKeyVector KeyRight = (1, 0)
playerKeyVector _ = (0, 0)

-- | Move the player using a pressed key.
movePlayer :: KeyPress -> Player -> Player
movePlayer key me =
  over character (moveCharacter 1.0 1 (playerKeyVector key))
  $ set
    canMove
    False
  me
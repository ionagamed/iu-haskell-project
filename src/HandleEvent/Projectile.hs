module HandleEvent.Projectile where

import Control.Lens
import Data.Fixed
import Framework.Types
import Math
import Types.Direction
import Types.Projectile
import Types.SmoothPosition

projectileDirectionVector :: Projectile -> V2
projectileDirectionVector me = directionToVector (projectileDirection me)

handleEventProjectile :: Event -> Projectile -> ActionCollector Projectile
handleEventProjectile (Update dt localDt) me =
  return $ moveProjectile localDt $
    me
      { projectilePosition = updateSmoothPosition (projectilePosition me) dt
      }
handleEventProjectile _ me = return me

moveProjectile :: Float -> Projectile -> Projectile
moveProjectile localDt me
  | projectileMoved me && dt > 0.5 = me {projectileMoved = False}
  | not (projectileMoved me) && dt < 0.5 =
    me
      { projectileMoved = True,
        projectilePosition =
          set time 1 $
            moveSmoothPosition (projectileDirectionVector me) (projectilePosition me)
      }
  | otherwise = me
  where
    dt = localDt `mod'` 0.25 * 4

-- | Event handler for `World`.
module HandleEvent.World where

import Control.Lens
import Data.Chain
import Framework.Types
import HandleEvent.Map
import Types.World

-- | Handle event for `World`.
handleEventWorld :: Event -> World -> ActionCollector World
handleEventWorld event =
  chainM
    [ overM currentMap (handleEventMap event),
      updatePassedTime event
    ]

-- | Update the stored timer with respect to delta time.  Uses the delta time
-- and not the actual timer, because on different environments the monotonic
-- clock might give weird results.
updatePassedTime :: Event -> World -> ActionCollector World
updatePassedTime (Update dt _) = return . over passedTime (+ dt)
updatePassedTime _ = return

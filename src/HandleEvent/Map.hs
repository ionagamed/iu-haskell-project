-- | Event handler for `Map`.
module HandleEvent.Map where

import Control.Lens
import Data.Chain
import Data.Maybe
import Framework.Types
import HandleEvent.Mob
import HandleEvent.Player
import HandleEvent.Projectile
import Interactions
import MeasureTime
import Types.Player
import Types.WorldMap

-- | Handle event for `Map`.
handleEventMap :: Event -> Map -> ActionCollector Map
handleEventMap event me =
  chainM
    [ updateInteractions,
      overM players (mapM (handleEventPlayer localEvent)),
      overM projectiles (mapM (handleEventProjectile localEvent)),
      overM blueSlimes (fmap catMaybes . mapM (handleEventBlueSlime localEvent)),
      overM chasers (fmap catMaybes . mapM (handleEventChaser localEvent (me ^. players))),
      addPlayers event
    ] me
  where
    localEvent =
      case event of
        (Update dt odt) -> Update dt (scaledMeasureLocalTime odt 1)
        x -> x

-- | Add new players as a reaction to an `AddPlayer` event.  Do nothing
-- otherwise.
addPlayers :: Event -> Map -> ActionCollector Map
addPlayers (AddPlayer clientId') me =
  return $
    over players ((:) (initPlayer clientId')) me
addPlayers _ me = return me

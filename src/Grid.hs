-- | Functions related to translating grid coordinates to screen coordinates.
--
-- Grid is composed of adjacent square tiles.
module Grid where

import Framework.Types

-- | Grid tile width and height.
gridSize :: Float
gridSize = 64

-- | Convert grid coords to world coords.  The world coords would be centered.
--
-- >>> gridToWorld (1, 4)
-- (96.0,288.0)
gridToWorld :: (Float, Float) -> (Float, Float)
gridToWorld (x, y) = (gridSize * x + gridSize / 2, gridSize * y + gridSize / 2)

-- | Convert a grid picture list to a screen picture list.
--
-- >>> import Data.Resources
-- >>> gridPicToWorldPic [(1, (1, 4), ImageResource "texture")]
-- [(1.0,(96.0,288.0),ImageResource "texture")]
gridPicToWorldPic :: [Picture] -> [Picture]
gridPicToWorldPic = map mapper
  where
    mapper (scale, pos, t) = (scale, gridToWorld pos, t)

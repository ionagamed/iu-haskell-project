{-# LANGUAGE DeriveGeneric #-}
{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Data.Resources where

import Data.Aeson.TH
import GHC.Generics (Generic)

-- | A Resource is basically a path to the file containing it.
type Resource = String

newtype MusicResource = MusicResource Resource deriving (Eq, Show, Generic)

newtype SoundResource = SoundResource Resource deriving (Eq, Show, Generic)

newtype ImageResource = ImageResource Resource deriving (Eq, Show, Generic)

$(deriveJSON defaultOptions ''MusicResource)

$(deriveJSON defaultOptions ''SoundResource)

$(deriveJSON defaultOptions ''ImageResource)

floorImage = ImageResource "./resources/floor-tile.jpg"

fffImage = ImageResource "resources/player.png"

fffRedImage = ImageResource "resources/player_red.png"

wallImage = ImageResource "resources/wall-tile.jpg"

projectileLeftImage = ImageResource "resources/projectile_left.png"

projectileRightImage = ImageResource "resources/projectile_right.png"

projectileDownImage = ImageResource "resources/projectile_down.png"

projectileUpImage = ImageResource "resources/projectile_up.png"

blankImage = ImageResource "resources/fff.jpg"

mobImage = ImageResource "resources/mob.png"

mobRedImage = ImageResource "resources/mob_red.png"

slimeImage = ImageResource "resources/slime.png"

slimeRedImage = ImageResource "resources/slime_red.png"

clipImage = ImageResource "resources/clip.jpg"

menuImage = ImageResource "resources/main_menu.png"

itemImage :: String -> ImageResource
itemImage s = ImageResource ("resources/item_" ++ s ++ ".png")

scatteredAndLost = MusicResource "resources/scattered-and-lost.ogg"

hurt1Sound = SoundResource "resources/hurt1.ogg"

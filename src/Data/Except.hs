module Data.Except where

import Control.Monad.Except

-- | Take a maybe value, and then if it's `Nothing`, throw an error with a
-- specified message, return the value in the `ExceptT` context otherwise.
--
-- >>> import Control.Monad.Identity
-- >>> runIdentity $ runExceptT (assertMaybeExcept (Just "a") "b" :: ExceptT String Identity String)
-- Right "a"
--
-- >>> import Control.Monad.Identity
-- >>> runIdentity $ runExceptT (assertMaybeExcept Nothing "b" :: ExceptT String Identity String)
-- Left "b"
assertMaybeExcept :: Monad m => Maybe a -> String -> ExceptT String m a
assertMaybeExcept maybeValue err =
  case maybeValue of
    Just value -> return value
    Nothing -> throwError err

-- | Assert a boolean assertion, throwing an error when the assertion is false.
--
-- >>> import Control.Monad.Identity
-- >>> runIdentity $ runExceptT (assertExcept True "b" :: ExceptT String Identity ())
-- Right ()
--
-- >>> import Control.Monad.Identity
-- >>> runIdentity $ runExceptT (assertExcept False "b" :: ExceptT String Identity ())
-- Left "b"
assertExcept :: Monad m => Bool -> String -> ExceptT String m ()
assertExcept True _ = return ()
assertExcept False err = throwError err

module Data.Cache where

import Control.Monad.State

-- | A basic key-value cache.
newtype Cache key value = Cache [(key, value)]
  deriving (Show)

-- | Empty cache of any type.
emptyCache :: Cache key value
emptyCache = Cache []

-- | Get a value from a raw cache.
--
-- >>> cacheGetRaw (Cache [("a", "b")]) "a"
-- Just "b"
--
-- >>> cacheGetRaw (Cache [("a", "b")]) "c"
-- Nothing
cacheGetRaw :: Eq key => Cache key value -> key -> Maybe value
cacheGetRaw (Cache assoc) key = lookup key assoc

-- | Put a value into the raw cache.  Note: not overriding the value, but rather
-- just inserting it, thus it will leak memory.
--
-- >>> cachePutRaw "a" "b" (Cache [])
-- Cache [("a","b")]
cachePutRaw :: key -> value -> Cache key value -> Cache key value
cachePutRaw key value (Cache assoc) = Cache ((key, value) : assoc)

-- | Get a wrapped clear cache into the state wrapper.
clearCache :: Monad m => StateT (Cache key value) m ()
clearCache = put emptyCache

-- | Fetch a resource from cache, loading it using an monadic value in case of a
-- miss.
--
-- >>> runStateT (fetchFromCache "key" (Just "value")) emptyCache
-- Just ("value",Cache [("key","value")])
--
-- >>> runStateT (fetchFromCache "key" (Just "value2")) (Cache [("key","value1")])
-- Just ("value1",...)
--
-- >>> runStateT (fetchFromCache "key" Nothing) (Cache [("key","value1")])
-- Just ("value1",...)
fetchFromCache ::
  (Monad m, Eq key) =>
  key ->
  m value ->
  StateT (Cache key value) m value
fetchFromCache key fetch = do
  cache <- get
  case cacheGetRaw cache key of
    Nothing -> do
      value <- lift fetch
      modify (cachePutRaw key value)
      return value
    Just value ->
      return value

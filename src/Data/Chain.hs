module Data.Chain where

import Control.Lens

-- | Chain a sequence of functions applied on the initial value.
--
-- The applications of `f`'s are done from the right.
--
-- >>> chain [\x -> x + 1, \x -> x + 2] 3
-- 6
chain :: [a -> a] -> a -> a
chain (f : fs) value = newValue
  where
    otherValue = chain fs value
    newValue = f otherValue
chain [] value = value

-- | Same as `chain`, but sequences the monads in order of application.
--
-- >>> chainM [\x -> Just (x + 1), \x -> Just (x + 2)] 3
-- Just 6
--
-- >>> chainM [\x -> Nothing, \x -> Just (x + 2)] 3
-- Nothing
chainM :: Monad m => [a -> m a] -> a -> m a
chainM (f : fs) value = do
  otherValue <- chainM fs value
  f otherValue
chainM [] value = return value

-- | Lift a monadic function from working on a part of a structure to working on
-- a whole structure using a lens.
--
-- >>> overM _1 (\x -> Just (x + 1)) (1, 2)
-- Just (2,2)
--
-- >>> overM _1 (\x -> Nothing) (1, 2)
-- Nothing
overM :: Monad m => Lens' s a -> (a -> m a) -> s -> m s
overM getter f s = do
  let a = s ^. getter
  newA <- f a
  return $ set getter newA s

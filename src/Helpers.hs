-- | A helper for some monadic computations that should happen only if some
-- value changes.
--
-- For example, if the current "seconds passed from the beginning of time"
-- changes, we want to execute some action.  If our function is computed many
-- times per second, it might become unwieldy to check the previous value every
-- time.
module Helpers where

import Control.Monad.State
import Data.HashMap.Strict

-- | A monad transformer that would keep some values and execute the underlying
-- action only if it changes.
type OnceWrapT var m a = StateT (HashMap String var) m a

-- | Check if the value changed, and if it did, return the new version and
-- remember it.  Return the old version otherwise.

-- $setup
-- >>> import qualified Data.HashMap.Strict as HM
--
-- When the state is empty, the new value is returned:
--
-- >>> runStateT (onChange "key1" 100 1 2) HM.empty
-- (2,...)
--
-- When first calling with the empty state, the key is remembered to contain the
-- value.
--
-- >>> runStateT (onChange "key1" 100 1 2 >> onChange "key1" 100 3 4) HM.empty
-- (3,...)
--
-- Different keys can be used to track different values:
--
-- >>> runStateT (onChange "key1" 100 1 2 >> onChange "key2" 100 3 4) HM.empty
-- (4,...)

onChange :: (Monad m, Eq var) => String -> var -> a -> a -> OnceWrapT var m a
onChange key var old new = do
  allValues <- get
  let maybeOldValue = Data.HashMap.Strict.lookup key allValues
  case maybeOldValue of
    Nothing -> do
      put $ insert key var allValues
      return new
    Just oldValue ->
      if oldValue == var
        then return old
        else do
          put $ insert key var allValues
          return new

{-# LANGUAGE DeriveGeneric #-}

-- | Type definitions for `World`.
module Types.World where

import Control.Lens
import Data.Aeson.TH
import GHC.Generics (Generic)
import Types.WorldMap

-- | A `World` is where everything in the game takes place.  Everything that
-- must be transmitted to the client (and containing the game state) is
-- contained here.
data World
  = World
      { _worldPassedTime :: Float,
        _worldCurrentMap :: Map,
        _worldMusicPlaying :: Bool
      }
  deriving (Generic, Show)

$(deriveJSON defaultOptions ''World)

makeFields ''World

-- | World state at the beginning.
initWorld :: World
initWorld =
  World
    { _worldPassedTime = 0,
      _worldCurrentMap = initMap,
      _worldMusicPlaying = False
    }

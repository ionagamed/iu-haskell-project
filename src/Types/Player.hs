{-# LANGUAGE DeriveGeneric #-}

module Types.Player where

import Control.Lens
import Data.Aeson.TH
import Framework.Types
import GHC.Generics (Generic)
import Types.Character
import Types.Items
import Types.SmoothPosition

-- | A player in the game.
data Player
  = Player
      { _playerCharacter :: Character,
        _playerCanMove :: Bool,
        _playerItems :: Items,
        _playerClientId :: ClientId
      }
  deriving (Show, Generic)

$(deriveJSON defaultOptions ''Player)

makeFields ''Player

-- | Initialize a player controlled by `controlledBy`.
initPlayer :: ClientId -> Player
initPlayer clientId' =
  Player
    { _playerCharacter = initCharacter (1, 1),
      _playerCanMove = False,
      _playerItems = [],
      _playerClientId = clientId'
    }

-- | Calculate the "display position" of the player (an interpolated position,
-- see `Types.SmoothPosition`.
playerDisplayPosition :: Player -> (Float, Float)
playerDisplayPosition me =
  intermediateSmoothPosition (me ^. character . position)

-- | Calculate the grid-locked current position of the player.
playerGridPosition :: Player -> (Float, Float)
playerGridPosition me = me ^. character . position . current

-- | Add an item to the player's inventory.
playerAddItem :: Player -> String -> Player
playerAddItem me item = set items (addItem item (me ^. items)) me

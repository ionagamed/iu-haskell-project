{-# LANGUAGE DeriveGeneric #-}

module Types.WorldMap where

import Control.Lens
import Data.Aeson.TH
import Framework.Types
import GHC.Generics (Generic)
import Types.Direction
import Types.Enemies.BlueSlime
import Types.Enemies.Chaser
import Types.FloorTile
import Types.WallTile
import Types.Items
import Types.Player
import Types.Projectile

data Map
  = Map
      { _mapFloorTiles :: [FloorTile],
        _mapWallTiles :: [WallTile],
        _mapProjectiles :: [Projectile],
        _mapItemTiles :: [ItemTile],
        _mapChasers :: [Chaser],
        _mapBlueSlimes :: [BlueSlime],
        _mapPlayers :: [Player]
      }
  deriving (Show, Generic)

$(deriveJSON defaultOptions ''Map)

makeFields ''Map

initMap :: Map
initMap =
  Map
    { _mapFloorTiles = defaultFloorTiles,
      _mapWallTiles = defaultWallTiles,
      _mapProjectiles = [],
      _mapItemTiles = defaultItemTiles,
      _mapPlayers = [],
      _mapChasers = [initChaser (4, 4), initChaser (9, 9)],
      _mapBlueSlimes = [initBlueSlime (8, 4) Types.Direction.Up, initBlueSlime (4, 8) Types.Direction.Left]
    }

findPlayerWithId :: ClientId -> Map -> Maybe Player
findPlayerWithId clientId' me =
  fmap fst $ uncons $ filter (\p -> clientId' == p ^. clientId) (me ^. players)

replacePlayerWithId :: ClientId -> Map -> Player -> Map
replacePlayerWithId clientId' me newPlayer =
  over players (map replacer) me
  where
    replacer player =
      if player ^. clientId == clientId'
        then newPlayer
        else player

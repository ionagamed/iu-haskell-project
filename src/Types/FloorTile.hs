{-# LANGUAGE DeriveGeneric #-}

module Types.FloorTile where

import Data.Aeson.TH
import Data.List
import GHC.Generics (Generic)

newtype FloorTile
  = FloorTile
      { floorTilePosition :: (Float, Float)
      }
  deriving (Show, Generic)

$(deriveJSON defaultOptions ''FloorTile)

initFloorTile :: (Float, Float) -> FloorTile
initFloorTile pos = FloorTile {floorTilePosition = pos}

defaultFloorTiles :: [FloorTile]
defaultFloorTiles = map initFloorTile coords
  where
    coords = vertical `union` horizontal
    vertical = [(x, y) | x <- [1 .. 10], y <- [1 .. 10]]
    horizontal = [(x, y) | x <- [1 .. 10], y <- [1 .. 10]]

-- | A module containing functions related to directions of movement and their
-- estimation.

{-# LANGUAGE DeriveGeneric #-}

module Types.Direction where

import Data.Aeson.TH
import GHC.Generics (Generic)
import Math
import Prelude hiding (Left, Right)

-- | A direction where something can move on the game grid.
data Direction
  = Up
  | Down
  | Left
  | Right
  | None
  deriving (Eq, Show, Generic)

$(deriveJSON defaultOptions ''Direction)

-- | Get a 2-vector corresponding to the specified `Direction`.
--
-- >>> directionToVector Up
-- (0.0,-1.0)
--
-- >>> directionToVector None
-- (0.0,0.0)
directionToVector :: Direction -> V2
directionToVector Up = (0, -1)
directionToVector Down = (0, 1)
directionToVector Left = (-1, 0)
directionToVector Right = (1, 0)
directionToVector None = (0, 0)

-- | Estimate a direction for a given 2-vector.
--
-- Comparison happens on the x coordinate first:
--
-- >>> estimateDirection (1,2)
-- Right
--
-- >>> estimateDirection (-1,2)
-- Left
--
-- The direction is supposed to happen in screen coordinates where larger y
-- values are lower:
--
-- >>> estimateDirection (0,2)
-- Down
--
-- When the vector is zero, we can only return the empty direction:
--
-- >>> estimateDirection (0,0)
-- None
estimateDirection :: V2 -> Direction
estimateDirection (x, y)
  | x > 0 = Right
  | x < 0 = Left
  | y < 0 = Up
  | y > 0 = Down
  | otherwise = None

-- | Get an opposite direction.
--
-- >>> oppositeDirection Left
-- Right
--
-- >>> oppositeDirection None
-- None
oppositeDirection :: Direction -> Direction
oppositeDirection dir
  | dir == Up = Down
  | dir == Down = Up
  | dir == Left = Right
  | dir == Right = Left
  | otherwise = None

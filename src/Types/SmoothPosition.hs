-- | `SmoothPosition` is a thing that can move between to discrete positions
-- fluidly, like players in the game that don't instantly appear on their next
-- tile when they are moving.

{-# LANGUAGE DeriveGeneric #-}

module Types.SmoothPosition where

import Control.Lens
import Data.Aeson.TH
import GHC.Generics (Generic)
import Math

data SmoothPosition
  = SmoothPosition
      { _smoothPositionPrevious :: (Float, Float),
        _smoothPositionCurrent :: (Float, Float),
        _smoothPositionTime :: Float
      }
  deriving (Show, Generic, Eq)

$(deriveJSON defaultOptions ''SmoothPosition)

makeFields ''SmoothPosition

-- | Create an empty `SmoothPosition` at the origin.
initSmoothPosition :: SmoothPosition
initSmoothPosition = initSmoothPositionAt (0, 0)

-- | Create an empty `SmoothPosition` at the specified coords.
initSmoothPositionAt :: (Float, Float) -> SmoothPosition
initSmoothPositionAt coords =
  SmoothPosition
    { _smoothPositionPrevious = coords,
      _smoothPositionCurrent = coords,
      _smoothPositionTime = 0
    }

-- | Get an intermediate coordinate value for a `SmoothPosition`.
intermediateSmoothPosition :: SmoothPosition -> (Float, Float)
intermediateSmoothPosition pos = currentPosition
  where
    transitionTime = min 1 (sqrt ((1 - pos ^. time) * 5))
    dPos = vMul (vSub (pos ^. current) (pos ^. previous)) transitionTime
    currentPosition = vAdd (pos ^. previous) dPos

-- | Swap current and previous positions.  Useful for something like bumping
-- into other objects on the map.
swapSmoothPosition :: SmoothPosition -> SmoothPosition
swapSmoothPosition me =
  set previous (me ^. current) $ set current (me ^. previous) me

-- | Move a `SmoothPosition` to a specified coordinate, remembering the old
-- value.
moveSmoothPosition :: (Float, Float) -> SmoothPosition -> SmoothPosition
moveSmoothPosition towards me =
  set previous (me ^. current)
    $ set current (vAdd (me ^. current) towards)
    $ set time 1 me

-- | Update a `SmoothPosition` with some passed time.
updateSmoothPosition :: SmoothPosition -> Float -> SmoothPosition
updateSmoothPosition pos dt = set time (max 0 (pos ^. time - dt)) pos

distance :: (Float, Float) -> (Float, Float) -> Float
distance (x1, y1) (x2, y2) = sqrt (x' * x' + y' * y')
    where
      x' = x1 - x2
      y' = y1 - y2
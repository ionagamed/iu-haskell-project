{-# LANGUAGE DeriveGeneric #-}

module Types.Projectile where

import Data.Aeson.TH
import GHC.Generics (Generic)
import Math
import Types.Direction
import Types.SmoothPosition

data Projectile
  = Projectile
      { projectilePosition :: SmoothPosition,
        projectileDirection :: Direction,
        projectileMoved :: Bool
      }
  deriving (Show, Generic)

$(deriveJSON defaultOptions ''Projectile)

initDefaultProjectile :: (Float, Float) -> Projectile
initDefaultProjectile position =
  Projectile
    { projectilePosition = initSmoothPositionAt position,
      projectileDirection = None,
      projectileMoved = False
    }

initProjectileTowards :: (Float, Float) -> V2 -> Maybe Projectile
initProjectileTowards position towards =
  fmap replaceDirection (extractDirection towards)
  where
    extractDirection (x, y)
      | x == 0 && y > 0 = Just Down
      | x == 0 && y < 0 = Just Up
      | x < 0 && y == 0 = Just Types.Direction.Left
      | x > 0 && y == 0 = Just Types.Direction.Right
      | otherwise = Nothing
    replaceDirection direction =
      (initDefaultProjectile position)
        { projectileDirection = direction
        }

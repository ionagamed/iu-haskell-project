{-# LANGUAGE DeriveGeneric #-}

module Types.Enemies.BlueSlime where

import Control.Lens
import Data.Aeson.TH
import GHC.Generics (Generic)
import Types.Direction
import Types.Mob

data BlueSlime
  = BlueSlime
      {_blueSlimeMob :: Mob}
  deriving (Show, Generic)

initBlueSlime :: (Float, Float) -> Direction -> BlueSlime
initBlueSlime pos dir =
  BlueSlime
    { _blueSlimeMob =
        (initMob pos)
          { _mobDirection = dir
          }
    }

$(deriveJSON defaultOptions ''BlueSlime)

makeFields ''BlueSlime

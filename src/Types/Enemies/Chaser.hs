{-# LANGUAGE DeriveGeneric #-}

module Types.Enemies.Chaser where

import Control.Lens
import Data.Aeson.TH
import GHC.Generics (Generic)
import Types.Mob

data Chaser
  = Chaser
      {_chaserMob :: Mob}
  deriving (Show, Generic)

initChaser :: (Float, Float) -> Chaser
initChaser pos =
  Chaser
    { _chaserMob = initMob pos
    }

$(deriveJSON defaultOptions ''Chaser)

makeFields ''Chaser

-- | A type for an in-game character.  Having a character means being able to
-- move dynamically.

{-# LANGUAGE DeriveGeneric #-}

module Types.Character where

import Control.Lens
import Data.Aeson.TH
import Data.Fixed
import GHC.Generics (Generic)
import Types.SmoothPosition
import Types.WallTile

data Character
  = Character
      { _characterPosition :: SmoothPosition,
        _characterHandledThisLoop :: Bool,
        _characterWaitBeats :: Int,
        _characterDamageAnimationTime :: Float
      }
  deriving (Show, Generic, Eq)

$(deriveJSON defaultOptions ''Character)

makeFields ''Character

-- | Initialize an empty `Character` at the initial position.
initCharacter :: (Float, Float) -> Character
initCharacter pos =
  Character
    { _characterPosition = initSmoothPositionAt pos,
      _characterHandledThisLoop = False,
      _characterWaitBeats = 1,
      _characterDamageAnimationTime = 0
    }

-- | Move a character towards a specific point.
moveCharacter :: Float -> Int -> (Float, Float) -> Character -> Character
moveCharacter localDt delay towards me
  | not (me ^. handledThisLoop) && not (characterCanMove me) && dt > 0.5 =
    set waitBeats (max 0 (me ^. waitBeats - 1)) $
      set
        handledThisLoop
        True
        me
  | characterCanMove me && canCharacterMoveTowards me towards && dt < 0.5 =
    set waitBeats delay
      $ set position (moveSmoothPosition towards (me ^. position))
      $ set
        handledThisLoop
        False
        me
  | dt < 0.5 =
    set handledThisLoop False me
  | otherwise = me
  where
    dt = localDt `mod'` 0.25 * 4

canCharacterMoveTowards :: Character -> (Float, Float) -> Bool
canCharacterMoveTowards me (x, y) = canMoveTowards (x', y')
  where
    x' = x + fst pos
    y' = y + snd pos
    pos = me ^. position . current

canMoveTowards :: (Float, Float) -> Bool
canMoveTowards tile
  | WallTile tile `elem` defaultWallTiles = False
  | otherwise = True

-- | Whether character can move now.
characterCanMove :: Character -> Bool
characterCanMove me = me ^. waitBeats == 0

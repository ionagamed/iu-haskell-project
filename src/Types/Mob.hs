{-# LANGUAGE DeriveGeneric #-}

module Types.Mob where

import Control.Lens
import Data.Aeson.TH
import GHC.Generics (Generic)
import Types.Character
import Types.Direction
import Types.Player -- For the lens overloading to work properly.
import Types.SmoothPosition

data Mob
  = Mob
      { _mobCharacter :: Character,
        _mobDirection :: Direction,
        _mobShouldDie :: Bool
      }
  deriving (Show, Generic)

$(deriveJSON defaultOptions ''Mob)

makeFields ''Mob

initMob :: (Float, Float) -> Mob
initMob pos =
  Mob
    { _mobCharacter = initCharacter pos,
      _mobDirection = Types.Direction.Left,
      _mobShouldDie = False
    }

mobDisplayPosition :: Mob -> (Float, Float)
mobDisplayPosition me = intermediateSmoothPosition (me ^. character . position)

mobGridPosition :: Mob -> (Float, Float)
mobGridPosition me = me ^. character . position . current

killMob :: Mob -> Mob
killMob = set shouldDie True

{-# LANGUAGE DeriveGeneric #-}

module Types.WallTile where

import Data.Aeson.TH
import Data.List
import GHC.Generics (Generic)

newtype WallTile
  = WallTile
      { wallTilePosition :: (Float, Float)
      }
  deriving (Show, Generic, Eq)
  
$(deriveJSON defaultOptions ''WallTile)

initWallTile :: (Float, Float) -> WallTile
initWallTile pos = WallTile { wallTilePosition = pos }

defaultWallTiles :: [WallTile]
defaultWallTiles = map initWallTile coords
  where
    coords = vertical `union` horizontal `union` obstacle1 `union` obstacle2
    vertical = [(x, y) | x <- [0 .. 11], y <- [0, 11]]
    horizontal = [(x, y) | x <- [0, 11], y <- [0 .. 11]]
    obstacle1 = [(x, y) | x <- [4], y <- [1 .. 7]]
    obstacle2 = [(x, y) | x <- [7], y <- [5 .. 10]]

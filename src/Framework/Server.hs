{-# OPTIONS_GHC -Wno-name-shadowing #-}

module Framework.Server where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad
import Framework.Types
import System.Clock

-- | Get a time value in seconds of the system's monotonic clock.
getTimeFloat :: IO Float
getTimeFloat = do
  timeSpec <- getTime Monotonic
  return (fromIntegral (toNanoSecs timeSpec) / 1000000000)

-- | Main runServer function.
runServer ::
  TVar world ->
  (Event -> world -> ActionCollector world) ->
  TQueue Event ->
  TQueue Action ->
  IO ()
runServer wrappedWorld handleEvent eventQueue actionQueue = do
  time <- getTimeFloat
  loop wrappedWorld handleEvent eventQueue actionQueue time

-- | Main game loop.
loop ::
  TVar world ->
  (Event -> world -> ActionCollector world) ->
  TQueue Event ->
  TQueue Action ->
  Float ->
  IO ()
loop wrappedWorld handleEvent eventQueue actionQueue lastTick = do
  threadDelay 16000
  time <- getTimeFloat
  let dt = time - lastTick
  world <- readTVarIO wrappedWorld
  newWorld <- loopHandleEvents dt time world handleEvent eventQueue actionQueue
  atomically $ writeTVar wrappedWorld newWorld
  loop wrappedWorld handleEvent eventQueue actionQueue time

-- | Event handling section of the main game loop.
loopHandleEvents ::
  Float ->
  Float ->
  world ->
  (Event -> world -> ActionCollector world) ->
  TQueue Event ->
  TQueue Action ->
  IO world
loopHandleEvents dt time world handleEvent eventQueue actionQueue = do
  keyPresses <- atomically $ flushTQueue eventQueue
  let events = Update dt time : keyPresses
  let (updatedWorld, actions) = handleEvents events handleEvent world
  atomically $ forM_ actions (writeTQueue actionQueue)
  return updatedWorld

-- | Collect all actions resulting from applying `handleEvent` on `world`, and
-- also return a new world.
handleEvents ::
  [Event] ->
  (Event -> world -> ActionCollector world) ->
  world ->
  (world, [Action])
handleEvents (event : events) handleEvent world =
  (newWorld, newActions ++ otherActions)
  where
    (otherWorld, otherActions) = handleEvents events handleEvent world
    (newWorld, newActions) = runActionCollector $ handleEvent event otherWorld
handleEvents [] _ world = (world, [])
-- vim: set ts=2 sw=2 fdm=marker:

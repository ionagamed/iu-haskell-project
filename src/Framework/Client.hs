{-# OPTIONS_GHC -Wno-name-shadowing #-}

-- | Client-side main game functions.
--
-- This module contains everything related to the game process, all networking
-- is handled by the runner in the main application module.
module Framework.Client where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.State
import Data.Cache
import Data.Default.Class (def)
import Data.Resources
import Foreign.C.Types
import Framework.Types
import qualified SDL
import qualified SDL.Image
import qualified SDL.Mixer

-- | A cache for `SoundResource` resources.
type SoundCache = Cache SoundResource SDL.Mixer.Chunk

-- | A cache for `MusicResource` resources.
type MusicCache = Cache MusicResource SDL.Mixer.Music

-- | A cache for `ImageResource` resources.
type ImageCache = Cache ImageResource SDL.Surface

-- | Screen width and height.
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (1024, 1024)

-- | Main runClient function.
runClient ::
  -- | A world "getter".
  TVar world ->
  -- | Outward key press stream.
  TQueue KeyPress ->
  -- | Inward action stream.
  TQueue Action ->
  -- | Draw function.
  (world -> [Picture]) ->
  IO ()
runClient wrappedWorld keyPressQueue actionQueue drawWorld = do
  (window, windowSurface) <- initializeWindow
  let (sounds, music, images) = initializeResources
  loop
    window
    windowSurface
    sounds
    music
    images
    wrappedWorld
    keyPressQueue
    actionQueue
    drawWorld
  SDL.destroyWindow window
  SDL.quit

-- | Initialize SDL and create a window.
initializeWindow :: IO (SDL.Window, SDL.Surface)
initializeWindow =
  do
    SDL.initialize [SDL.InitVideo, SDL.InitAudio]
    SDL.Mixer.openAudio def 256
    SDL.Mixer.setMusicVolume 20
    SDL.Mixer.initialize [SDL.Mixer.InitOGG]
    window <-
      SDL.createWindow
        "My awsum game"
        SDL.defaultWindow
          { SDL.windowInitialSize = SDL.V2 screenWidth screenHeight,
            SDL.windowGraphicsContext = SDL.OpenGLContext SDL.defaultOpenGL
          }
    SDL.showWindow window
    _ <- SDL.glCreateContext window
    windowSurface <- SDL.getWindowSurface window
    return (window, windowSurface)

-- | Initialize resource cache lists.
initializeResources :: (SoundCache, MusicCache, ImageCache)
initializeResources = (emptyCache, emptyCache, emptyCache)

-- | Fetch an image resource by either locating it in the cache or loading it
-- just in time (storing it in the cache on the way).
fetchImage :: ImageResource -> StateT ImageCache IO SDL.Surface
fetchImage (ImageResource path) =
  fetchFromCache
    (ImageResource path)
    (SDL.Image.load path :: IO SDL.Surface)

-- | Fetch a sound resource by either locating it in the cache or loading it
-- just in time (storing it in the cache on the way).
fetchSound :: MonadIO m => SoundResource -> StateT SoundCache m SDL.Mixer.Chunk
fetchSound (SoundResource path) =
  fetchFromCache
    (SoundResource path)
    (liftIO (SDL.Mixer.load path :: IO SDL.Mixer.Chunk))

-- | Fetch a music resource by either locating it in the cache or loading it
-- just in time (storing it in the cache on the way).
fetchMusic :: MusicResource -> StateT MusicCache IO SDL.Mixer.Music
fetchMusic (MusicResource path) =
  fetchFromCache
    (MusicResource path)
    (SDL.Mixer.load path :: IO SDL.Mixer.Music)

-- | Main game loop.
-- TODO: refactor 10+ arguments into a separate state.
loop ::
  SDL.Window ->
  SDL.Surface ->
  SoundCache ->
  MusicCache ->
  ImageCache ->
  TVar world ->
  TQueue KeyPress ->
  TQueue Action ->
  (world -> [Picture]) ->
  IO ()
loop
  window
  windowSurface
  sounds
  music
  images
  wrappedWorld
  keyPressQueue
  actionQueue
  drawWorld =
    do
      threadDelay 16000
      quit <- dumpKeysToServer keyPressQueue
      actions <- atomically $ flushTQueue actionQueue
      (newSounds, newMusic) <- handleActions sounds music actions
      SDL.surfaceFillRect windowSurface Nothing (SDL.V4 0 0 0 255)
      world <- readTVarIO wrappedWorld
      newImages <-
        flip execStateT images $
          mapM (draw windowSurface) (drawWorld world)
      -- SDL.glSwapWindow window
      SDL.updateWindowSurface window
      unless
        quit
        ( loop
            window
            windowSurface
            newSounds
            newMusic
            newImages
            wrappedWorld
            keyPressQueue
            actionQueue
            drawWorld
        )

-- | Get all keys from the player, and dump them into the send queue.
dumpKeysToServer :: TQueue KeyPress -> IO Bool
dumpKeysToServer keyPressQueue = do
  events <- SDL.pollEvents
  let quit = elem SDL.QuitEvent $ map SDL.eventPayload events
  let keyPresses = mapEvents events
  _ <- atomically $ forM keyPresses (writeTQueue keyPressQueue)
  return quit

-- | Handle all actions in a list, producing a new cache.
handleActions ::
  SoundCache ->
  MusicCache ->
  [Action] ->
  IO (SoundCache, MusicCache)
handleActions sounds music actions =
  flip runStateT music
    $ flip execStateT sounds
    $ mapM evalAction actions

-- | Map all SDL events to engine keypresses.
mapEvents :: [SDL.Event] -> [KeyPress]
mapEvents [] = []
mapEvents (event : events) = outputEvents
  where
    payload = SDL.eventPayload event
    tailEvents = mapEvents events
    maybeTargetEvent =
      case payload of
        SDL.KeyboardEvent (SDL.KeyboardEventData _ SDL.Pressed _ (SDL.Keysym code _ _)) ->
          mapCode code
        _ -> Nothing
    outputEvents =
      case maybeTargetEvent of
        Just event -> event : tailEvents
        Nothing -> tailEvents

-- | Map SDL scancode to a `Maybe KeyPress` (some scancodes might not have a
-- mapping).
mapCode :: SDL.Scancode -> Maybe KeyPress
mapCode SDL.ScancodeUp = Just KeyUp
mapCode SDL.ScancodeW = Just KeyUp
mapCode SDL.ScancodeDown = Just KeyDown
mapCode SDL.ScancodeS = Just KeyDown
mapCode SDL.ScancodeLeft = Just KeyLeft
mapCode SDL.ScancodeA = Just KeyLeft
mapCode SDL.ScancodeRight = Just KeyRight
mapCode SDL.ScancodeD = Just KeyRight
mapCode SDL.ScancodeReturn = Just KeyEnter
mapCode _ = Nothing

-- | Draw a `Picture` onto the `target`, loading images on demand.
draw :: SDL.Surface -> Picture -> StateT ImageCache IO ()
draw target (s, (x, y), name) = do
  texture <- fetchImage name
  (SDL.V2 w h) <- SDL.surfaceDimensions texture
  let targetW = round (fromIntegral w * s)
  let targetH = round (fromIntegral h * s)
  let targetX = (round x - targetW `div` 2) :: CInt
  let targetY = (round y - targetH `div` 2) :: CInt
  SDL.surfaceBlitScaled
    texture
    Nothing
    target
    ( Just $
        SDL.Rectangle
          (SDL.P $ SDL.V2 targetX targetY)
          (SDL.V2 targetW targetH)
    )
  return ()

-- | Evaluate an `Action`, loading resources on demand.
evalAction :: Action -> StateT SoundCache (StateT MusicCache IO) ()
evalAction (PlayMusic musicResource) = do
  music <- lift $ fetchMusic musicResource
  SDL.Mixer.playMusic SDL.Mixer.Once music
evalAction (PlaySound soundResource) = do
  sound <- fetchSound soundResource
  SDL.Mixer.play sound
evalAction (Print string) = liftIO $ putStrLn string
evalAction _ = return ()
-- vim: set ts=2 sw=2 fdm=marker:

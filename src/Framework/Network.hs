-- | Contains networking common definitions for both clients and servers.
module Framework.Network where

import Control.Monad.Except
import Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Network.WebSockets as WS

-- | Except monad, which would close the connection with the peer, sending him a
-- message with the reason.
type WSExcept = ExceptT BS.ByteString IO

-- | Assert a boolean assertion, throwing the error into `WSExcept` when the
-- assertion is false, and returning `()` otherwise.
assertWSExcept :: Bool -> BS.ByteString -> WSExcept ()
assertWSExcept True _ = return ()
assertWSExcept False err = throwError err

-- | Unwrap the `WSExcept` computation, catching any errors and returning a
-- `Maybe` output value (due to it not being available when the computation has
-- failed).
runWSExcept :: WS.Connection -> WSExcept a -> IO (Maybe a)
runWSExcept connection wsExcept = do
  result <- runExceptT wsExcept
  case result of
    Right x -> return $ Just x
    Left err -> WS.sendClose connection err >> return Nothing

-- | Unwrap the `WSExcept` computation, catching any errors, but discarding the
-- return value.  See `runWSExcept`.
execWSExcept :: WS.Connection -> WSExcept a -> IO ()
execWSExcept connection wsExcept = void $ runWSExcept connection wsExcept

-- | Send any JSON-decodable message into the `Connection`.
sendMessage :: ToJSON m => WS.Connection -> m -> IO ()
sendMessage connection message = do
  let rawMessage = encode message
  WS.sendTextData connection rawMessage

-- | Receive any JSON-decodable message from the `Connection`, wrapped in a
-- `WSExcept` computation, which would fail when the data received is not in
-- valid format.
recvMessage :: FromJSON m => WS.Connection -> WSExcept m
recvMessage connection = do
  rawMessage <- lift $ WS.receiveData connection :: WSExcept BS.ByteString
  let maybeMessage = decode rawMessage :: FromJSON m => Maybe m
  case maybeMessage of
    Just message -> return message
    Nothing -> throwError "Wrong input received on websocket input"

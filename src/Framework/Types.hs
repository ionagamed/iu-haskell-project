-- | Types for basic engine operations.
--
-- An `Event` corresponds to something that happened in the outside world (e.g.
-- some time passed or a key was pressed).
--
-- A `Picture` corresponds to one draw call in the engine, but pictures are
-- supposed to be combined, hence the implementation here is that a single
-- `texture` call returns a singleton list.
--
-- An `Action` is something that pure game code wants the engine to do (e.g.
-- play some sound effect, or print a debug string).
--
-- `Action`s are collected from the game code using a writer monad
-- `ActionCollector`.  For example, to print a string, updating some tuple, the
-- following is supposed:
--
-- @
--    handleEventTuple :: Event -> (Int, Int) -> ActionCollector (Int, Int)
--    handleEventTuple _ (x, y) = do
--      perform $ Print $ show x
--      return (x + 1, y)
-- @

{-# LANGUAGE DeriveGeneric #-}

module Framework.Types where

import Control.Monad.Identity
import Control.Monad.Writer.Lazy
import Data.Aeson.TH
import Data.Hashable
import Data.Resources
import GHC.Generics (Generic)

-- | A networked client unique id to distinguish them.
newtype ClientId = ClientId Int
  deriving (Show, Eq, Generic)

-- | Key press event that is going to be sent to the application
data KeyPress = KeyLeft | KeyRight | KeyUp | KeyDown | KeyEnter
  deriving (Show)

$(deriveJSON defaultOptions ''KeyPress)

-- | Event that is going to trigger some change and actions in the underlying
-- application data.
data Event
  = -- | A client pressed a key.
    KeyPressOf ClientId KeyPress
  | -- | Some time has passed, time to update.
    Update Float Float
  | -- | A new client has arrived.
    AddPlayer ClientId
  deriving (Show)

-- | Output action from passing events.
data Action
  = PlayMusic MusicResource
  | PlaySound SoundResource
  | Print String
  | SetClientId ClientId
  deriving (Show, Generic)

$(deriveJSON defaultOptions ''Action)

-- | Output picture drawn on the screen.
type Picture = (Float, (Float, Float), ImageResource)

-- | Create a basic scale-1 texture from a resource name.
--
-- >>> texture (ImageResource "name")
-- [(1.0,(0.0,0.0),ImageResource "name")]
texture :: ImageResource -> [Picture]
texture x = [(1, (0, 0), x)]

-- | Transated picture
--
-- >>> translated (10, 20) $ texture (ImageResource "name")
-- [(1.0,(10.0,20.0),ImageResource "name")]
--
-- Mulitple `translated` calls stack:
--
-- >>> translated (10, 20) $ translated (30, 40) $ texture (ImageResource "name")
-- [(1.0,(40.0,60.0),ImageResource "name")]
translated :: (Float, Float) -> [Picture] -> [Picture]
translated (dx, dy) = map mapper
  where
    mapper (scale, (x, y), t) = (scale, (x + dx, y + dy), t)

-- | Scaled picture
--
-- >>> scaled 5 $ texture (ImageResource "name")
-- [(5.0,(0.0,0.0),ImageResource "name")]
--
-- Multiple `scaled` calls stack:
--
-- >>> scaled 2 $ scaled 3 $ texture (ImageResource "name")
-- [(6.0,(0.0,0.0),ImageResource "name")]
scaled :: Float -> [Picture] -> [Picture]
scaled s = map mapper
  where
    mapper (scale, (x, y), t) = (scale * s, (x, y), t)

-- | Action collector, which allows for simpler handleEvent recursive calls.
type ActionCollector a = WriterT [Action] Identity a

-- | Log an action into the ActionCollector.  Implemented to hide the
-- implementation as it might change later due to my lack of experience with
-- monad transformers.
perform :: Action -> ActionCollector ()
perform action = tell [action]

-- | Run the ActionCollector value, returning the resulting value and all
-- collected actions.
runActionCollector :: ActionCollector a -> (a, [Action])
runActionCollector = runIdentity . runWriterT

-- | Client portion of "every-frame" exchange.
data ClientExchange = ClientExchange [KeyPress]
  deriving (Generic, Show)

-- | Server portion of "every-frame" exchange.
data ServerExchange world = ServerExchange world [Action]
  deriving (Generic, Show)

$(deriveJSON defaultOptions ''ClientExchange)

$(deriveJSON defaultOptions ''ServerExchange)

-- | A default client id, to be inserted into events that go from the client to
-- the server.
placeholderClientId :: ClientId
placeholderClientId = ClientId (-1)

instance Hashable ClientId

$(deriveJSON defaultOptions ''ClientId)

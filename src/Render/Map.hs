-- | Render function for `Map`.
module Render.Map where

import Control.Lens
import Framework.Types
import Grid
import Math
import Render.WallTile
import Render.FloorTile
import Render.ItemTile
import Render.Mob
import Render.Player
import Render.Projectile
import Types.Character
import Types.Player
import Types.SmoothPosition
import Types.WorldMap

-- | Render a `Map` for the specified `ClientId`.
renderMap :: ClientId -> Map -> [Picture]
renderMap clientId' me = pic
  where
    currentPlayer = findPlayerWithId clientId' me
    cameraOffset =
      case currentPlayer of
        Just p -> vNeg $ intermediateSmoothPosition (p ^. character . position)
        Nothing -> (0, 0)
    pic =
      (gridPicToWorldPic . translated cameraOffset)
        ( concatMap renderFloorTile (me ^. floorTiles)
            ++ concatMap renderWallTile (me ^. wallTiles)
            ++ concatMap renderProjectile (me ^. projectiles)
            ++ concatMap renderItemTile (me ^. itemTiles)
            ++ concatMap renderChaser (me ^. chasers)
            ++ concatMap renderBlueSlime (me ^. blueSlimes)
            ++ concatMap renderPlayer (me ^. players)
        )

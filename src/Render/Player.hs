module Render.Player where

import Control.Lens
import Data.Resources
import Framework.Types
import Types.Character
import Types.Player

renderPlayer :: Player -> [Picture]
renderPlayer me =
  scaled (playerCurrentScale me * 0.8) $
    translated (playerDisplayPosition me) pic
  where
    pic =
      texture $
        if me ^. character . damageAnimationTime > 0
          then fffRedImage
          else fffImage

playerCurrentScale :: Player -> Float
playerCurrentScale me = scale
  where
    scale = 0.4 + 0.2 * max 0.6 (sin (pi * (me ^. character . damageAnimationTime) / 4))

module Render.Projectile where

import Data.Resources
import Framework.Types
import Types.Direction
import Types.Projectile
import Types.SmoothPosition

renderProjectile :: Projectile -> [Picture]
renderProjectile me = translated currentPosition $ texture name
  where
    currentPosition = intermediateSmoothPosition $ projectilePosition me
    name
      | projectileDirection me == Types.Direction.Left = projectileLeftImage
      | projectileDirection me == Types.Direction.Right = projectileRightImage
      | projectileDirection me == Up = projectileUpImage
      | projectileDirection me == Down = projectileDownImage
      | otherwise = blankImage

module Render.ItemsHUD where

import Framework.Types
import Grid
import Render.ItemTile
import Types.Items

renderItemsHUD :: Items -> [Picture]
renderItemsHUD items = gridPicToWorldPic $ mconcat itemPictures
  where
    itemPictures = map renderItemTile itemTiles
    itemTiles = zipWith makeItemTile [0 ..] items
    makeItemTile offset = ItemTile (offset, 0)

-- | Render function for `World`.
module Render.World where

import Control.Lens
import Data.Resources
import Framework.Types
import MeasureTime
import Render.Map
import Render.Pacemaker
import Types.World

-- | Render a `World`.
renderWorld :: ClientId -> World -> [Picture]
renderWorld clientId' world =
  concat [mapPics, worldClips, renderPacemaker localDt, itemsHUD]
  where
    -- TODO: do you believe in magic
    localDt =
      scaledMeasureLocalTime (world ^. passedTime) 1
    mapPics =
      translated (512 - 32, 512 - 32) $
        renderMap clientId' (world ^. currentMap)
    -- we need to clip the world so it doesn't go out of the boundaries
    worldClip = translated (512, 32) $ texture clipImage
    worldClips = worldClip <> translated (0, 1024 - 64) worldClip
    -- rendering the HUDs
    itemsHUD = []
-- itemsHUD = renderItemsHUD (world^.currentMap.player.items)

module Render.FloorTile where

import Data.Resources
import Framework.Types
import Types.FloorTile

renderFloorTile :: FloorTile -> [Picture]
renderFloorTile me =
  scaled 0.5 $ translated (floorTilePosition me) $ texture floorImage

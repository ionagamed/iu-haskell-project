module Render.Mob where

import Control.Lens
import Data.Resources
import Framework.Types
import Types.Enemies.BlueSlime
import Types.Enemies.Chaser
import Types.Mob

renderBlueSlime :: BlueSlime -> [Picture]
renderBlueSlime me = scaled scale $ translated pos $ texture name
  where
    scale = 0.8
    pos = mobDisplayPosition $ me ^. Types.Enemies.BlueSlime.mob
    name
      | me ^. Types.Enemies.BlueSlime.mob . shouldDie = slimeRedImage
      | otherwise = slimeImage

renderChaser :: Chaser -> [Picture]
renderChaser me = scaled scale $ translated pos $ texture name
  where
    scale = 0.8
    pos = mobDisplayPosition $ me ^. Types.Enemies.Chaser.mob
    name
      | me ^. Types.Enemies.Chaser.mob . shouldDie = mobRedImage
      | otherwise = mobImage

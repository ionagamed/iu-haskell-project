module Render.Pacemaker where

import Data.Fixed
import Data.Resources
import Framework.Types

renderPacemaker :: Float -> [Picture]
renderPacemaker localDt = translated (512, 1024) pacemaker
  where
    pacemaker = scaled scale $ texture fffImage
    dt = (localDt `mod'` pace) * (2 / pace)
    scale = 0.5 * max 0.8 (cos dt)
    pace = 0.25

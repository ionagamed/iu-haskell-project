module Render.ItemTile where

import Data.Resources
import Framework.Types
import Types.Items

renderItemTile :: ItemTile -> [Picture]
renderItemTile me = pic
  where
    pic = translated position $ scaled scale $ texture (itemImage (itemTileName me))
    scale = 0.8
    position = itemTilePosition me

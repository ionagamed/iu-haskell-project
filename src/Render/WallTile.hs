module Render.WallTile where

import Data.Resources
import Framework.Types
import Types.WallTile

renderWallTile :: WallTile -> [Picture]
renderWallTile me = 
  scaled 0.5 $ translated (wallTilePosition me) $ texture wallImage
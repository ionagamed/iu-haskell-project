module Main where

import Data.Resources
import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad.Except
import Data.Except
import Data.List
import Framework.Client
import Framework.Network
import Framework.Types
import qualified Network.WebSockets as WS
import Render.World
import System.Environment
import Types.World

-- | Parse the server address from environment variable "SERVER".
parseHost :: IO (Either String (String, Int))
parseHost = runExceptT $ do
  let err = "SERVER should be in env in format 'host:port'"
  maybeAddress <- lift $ lookupEnv "SERVER"
  address <- assertMaybeExcept maybeAddress err
  assertExcept (':' `elem` address) err
  let host = takeWhile (/= ':') address
  let port = read $ dropWhile (== ':') $ dropWhile (/= ':') address :: Int
  return (host, port)

runClientNetworkThread :: TMVar ClientId -> IO (TQueue KeyPress, TQueue Action, TVar World)
runClientNetworkThread clientId = do
  actionQueue <- newTQueueIO
  keyPressQueue <- newTQueueIO
  wrappedWorld <- newTVarIO initWorld :: IO (TVar World)
  eitherServer <- parseHost
  case eitherServer of
    Left err -> putStrLn ("Not using multiplayer: " ++ err)
    Right server -> do
      putStrLn ("Nice, using multiplayer on " ++ show server)
      _ <- forkIO $ clientThreadMain keyPressQueue actionQueue wrappedWorld clientId server
      return ()
  return (keyPressQueue, actionQueue, wrappedWorld)

-- | The main function for the client thread.
clientThreadMain ::
  TQueue KeyPress ->
  TQueue Action ->
  TVar World ->
  TMVar ClientId ->
  -- | Server address (host, port).
  (String, Int) ->
  IO ()
clientThreadMain keyPressQueue actionQueue wrappedWorld clientId (host, port) = do
  atomically $ writeTQueue actionQueue $ PlayMusic scatteredAndLost
  WS.runClient host port "/" $
    \connection ->
      void $
        runWSExcept
          connection
          (clientLoop keyPressQueue actionQueue wrappedWorld clientId connection)

-- | Is the acion of type `SetClientId`.
isSetClientId :: Action -> Bool
isSetClientId (SetClientId _) = True
isSetClientId _ = False

-- | Extract a `SetClientId` action from a `TQueue`, without modifying the
-- queue.
extractClientId :: TQueue Action -> IO (Maybe ClientId)
extractClientId actionQueue =
  atomically $ do
    actions <- flushTQueue actionQueue
    let clientIdActions = filter isSetClientId $ reverse actions
    forM_ actions (writeTQueue actionQueue)
    let maybeSetClientId = uncons clientIdActions
    case maybeSetClientId of
      Just (SetClientId x, _) -> return (Just x)
      _ -> return Nothing

-- | The main loop for the client thread.
clientLoop ::
  TQueue KeyPress ->
  TQueue Action ->
  TVar World ->
  TMVar ClientId ->
  WS.Connection ->
  WSExcept ()
clientLoop keyPressQueue actionQueue wrappedWorld clientId connection = do
  keyPresses <- lift $ atomically $ flushTQueue keyPressQueue
  lift $ sendMessage connection (ClientExchange keyPresses)
  (ServerExchange world actions) <- recvMessage connection
  lift $ atomically $ writeTVar wrappedWorld world
  _ <- lift $ atomically $ forM actions (writeTQueue actionQueue)
  maybeClientId <- lift $ extractClientId actionQueue
  case maybeClientId of
    Just x -> void $ lift $ atomically $ tryPutTMVar clientId x
    Nothing -> return ()
  clientLoop keyPressQueue actionQueue wrappedWorld clientId connection

-- | The client's main function.
main :: IO ()
main = do
  wrappedClientId <- newEmptyTMVarIO
  (keyPressQueue, actionQueue, wrappedWorld) <- runClientNetworkThread wrappedClientId
  clientId <- atomically $ takeTMVar wrappedClientId
  runClient wrappedWorld keyPressQueue actionQueue (renderWorld clientId)
-- vim: set ts=2 sw=2 fdm=marker:

-- | This is a server with all game logic.
module Main where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad.Except
import Data.Except
import qualified Data.HashMap.Strict as HM
import Framework.Network
import Framework.Server (runServer)
import Framework.Types
import HandleEvent.World
import qualified Network.WebSockets as WS
import System.Environment
import Types.World

-- | A client record in the server state.
type Client = WS.Connection

-- | Server's client associative list.
-- type Clients = [(ClientId, Client)]
type Clients = HM.HashMap ClientId Client

-- | Server state w.r.t. clients.
data ServerState
  = ServerState
      { serverClients :: Clients,
        nextClientId :: ClientId
      }

-- | Empty server state, used when the server just starts.
emptyState :: ServerState
emptyState =
  ServerState
    { serverClients = HM.empty,
      nextClientId = ClientId 1
    }

-- | Add a client to the list, without actually blocking anything.  This is not
-- supposed to be used from usual code, there is `addClient` which wraps this
-- into an STM transaction.
addClientUnsafe :: WS.Connection -> ServerState -> (ClientId, ServerState)
addClientUnsafe connection state = (ClientId clientId, newState)
  where
    packedId@(ClientId clientId) = nextClientId state
    newState =
      state
        { serverClients = HM.insert packedId connection (serverClients state),
          nextClientId = ClientId (clientId + 1)
        }

-- | Safely (atomically) add a new client into the state.
addClient :: WS.Connection -> TVar ServerState -> STM ClientId
addClient connection serverState =
  stateTVar serverState $ addClientUnsafe connection

-- | Main server loop.
loop ::
  TQueue Event ->
  TQueue Action ->
  WS.Connection ->
  ClientId ->
  TVar World ->
  WSExcept ()
loop eventQueue actionQueue connection clientId wrappedWorld = do
  (ClientExchange keyPresses) <- recvMessage connection
  let events = map (KeyPressOf clientId) keyPresses
  _ <- lift $ atomically $ mapM (writeTQueue eventQueue) events
  actions <- lift $ atomically $ flushTQueue actionQueue
  world <- lift $ readTVarIO wrappedWorld
  lift $ sendMessage connection (ServerExchange world actions)
  loop eventQueue actionQueue connection clientId wrappedWorld

-- | Parse the server listening host and port from environment variable
-- `ADDRESS`.
parseHost :: IO (Either String (String, Int))
parseHost = runExceptT $ do
  let err = "ADDRESS should be in env in format 'host:port'"
  maybeAddress <- lift $ lookupEnv "ADDRESS"
  address <- assertMaybeExcept maybeAddress err
  assertExcept (':' `elem` address) err
  let host = takeWhile (/= ':') address
  let port = read $ dropWhile (== ':') $ dropWhile (/= ':') address :: Int
  return (host, port)

-- | Server main function.
main :: IO ()
main = do
  eitherHost <- Main.parseHost
  (host, port) <- case eitherHost of
    Left err -> error err
    Right address -> return address
  eventQueue <- newTQueueIO :: IO (TQueue Event)
  actionQueue <- newTQueueIO :: IO (TQueue Action)
  wrappedWorld <- newTVarIO initWorld
  _ <- forkIO $ runServer wrappedWorld handleEventWorld eventQueue actionQueue
  state <- newTVarIO emptyState
  putStrLn ("Started listening on " ++ host ++ ":" ++ show port)
  WS.runServer host port $ \pending -> do
    connection <- WS.acceptRequest pending
    newClientId <- atomically $ addClient connection state
    atomically $ writeTQueue eventQueue (AddPlayer newClientId)
    world <- readTVarIO wrappedWorld
    sendMessage connection (ServerExchange world [SetClientId newClientId])
    putStrLn ("New connection, assigning id " ++ show newClientId)
    _ <- runWSExcept connection $ loop eventQueue actionQueue connection newClientId wrappedWorld
    return ()
-- vim: set fdm=marker:

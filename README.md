# Crypt of the Necrodancer Ripoff

This will be a rhythm gaem lul.

## Install

### Native libraries

This project uses stack, but some libraries depend on C libraries, so a C
compiler would be nice, and also follow further platform-specific steps to
install the required libraries.

If you have a system that is not listed here, please somehow install `sdl2`,
`sdl2-image` and `sdl2-mixer` with development headers.

#### macOS

Brew can do the work for you:

```
$ brew install pkg-config
$ brew install sdl2 sdl2_image sdl2_mixer
```

#### Ubuntu

Seems to work for 18.04:

```
$ sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev
```

### Haskell dependencies

These can be installed with stack (will also build the project for you):

```
$ stack build
```

## Run

This game requires both a working client and a working server.

Running through `stack` for development:

```
$ ADDRESS=127.0.0.1:9000 stack run server-exe
$ SERVER=127.0.0.1:9000 stack run game-exe
```

## Testing

Doctests can be run with `stack` somehow, but I couldn't get it to work, so to
run doctests you can use a bundled shell script:

```
$ ./test/doctest.sh
```

Usual unit tests can be executed as usual:

```
$ stack test
```

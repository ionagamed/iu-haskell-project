{- HLINT ignore "Use camelCase" -}
{- HLINT ignore "Redundant do" -}

module FrameworkTypesSpec where

import Data.Resources
import Framework.Types
import Math
import Test.Hspec
import Test.QuickCheck

texture_prop :: String -> Bool
texture_prop value =
  texture (ImageResource value) == [(1, (0, 0), ImageResource value)]

translated_basic_prop :: (Float, Float) -> String -> Bool
translated_basic_prop vec value =
  translated vec (texture (ImageResource value)) == [(1, vec, ImageResource value)]

translated_stacking_prop :: (Float, Float) -> (Float, Float) -> String -> Bool
translated_stacking_prop v1 v2 value =
  translated v2 (translated v1 (texture (ImageResource value))) == [(1, vAdd v1 v2, ImageResource value)]

scaled_basic_prop :: Float -> String -> Bool
scaled_basic_prop scale' value =
  scaled scale' (texture (ImageResource value)) == [(scale', (0, 0), ImageResource value)]

scaled_stacking_prop :: Float -> Float -> String -> Bool
scaled_stacking_prop s1 s2 value =
  scaled s1 (scaled s2 (texture (ImageResource value))) == [(s1 * s2, (0, 0), ImageResource value)]


spec :: Spec
spec = do
  describe "texture" $ do
    it "should keep the texture name" $ property
      texture_prop
  describe "translated" $ do
    it "should translate to vector from the origin" $ property
      translated_basic_prop
    it "should apply both translate calls" $ property
      translated_stacking_prop
  describe "scaled" $ do
    it "should scale to vector from the origin" $ property
      scaled_basic_prop
    it "should apply both scale calls" $ property
      scaled_stacking_prop

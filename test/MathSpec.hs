{- HLINT ignore "Use camelCase" -}
{- HLINT ignore "Redundant do" -}

module MathSpec where

import Math
import Test.Hspec
import Test.QuickCheck

vPrecision :: Float
vPrecision = 10 ** (-9)

(.==) :: V2 -> V2 -> Bool
(.==) (x1, y1) (x2, y2) = abs (x1 - x2) <= vPrecision && abs (y1 - y2) <= vPrecision

add_sub_prop :: V2 -> V2 -> Bool
add_sub_prop v1 v2 =
  vAdd v2 (vSub v1 v2) .== v1

mul_inverse_prop :: Property
mul_inverse_prop =
  forAll
    (arbitrary `suchThat` (> 0))
    ( \s ->
        forAll
          arbitrary
          (\vec -> vMul (vMul vec s) (1 / s) .== vec)
    )

add_neg_zero_prop :: V2 -> Bool
add_neg_zero_prop vec =
  vAdd (vNeg vec) vec .== (0, 0)

spec :: Spec
spec = do
  xit "v1 + v2 - v2 == v1" $ property
    add_sub_prop
  xit "v * a * (1/a) == v" $
    mul_inverse_prop
  it "v + (-v) == 0" $ property
    add_neg_zero_prop

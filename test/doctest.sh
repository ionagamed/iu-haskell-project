#!/bin/bash

SCRIPTPATH=$0
SCRIPTDIR=$(dirname $SCRIPTPATH)
REPODIR=$(dirname $SCRIPTDIR)

pushd $REPODIR
stack exec doctest -- -XTemplateHaskell -XMultiParamTypeClasses -XFunctionalDependencies -XFlexibleInstances -XRankNTypes -XOverloadedStrings -isrc src/**/*.hs

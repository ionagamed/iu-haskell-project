{- HLINT ignore "Use camelCase" -}
{- HLINT ignore "Redundant do" -}

module MobSpec where

import Control.Lens

import Test.Hspec
import Test.QuickCheck

import Types.Mob
import Types.Direction
import Prelude hiding (Left)


initMob_setsDefaultDirection_prop :: (Float, Float) -> Bool
initMob_setsDefaultDirection_prop pos =
  initMob pos ^. direction == Left
  
initMob_setsShouldDie_prop :: (Float, Float) -> Bool
initMob_setsShouldDie_prop pos = 
  not $ initMob pos ^. shouldDie   
 
killMob_setsShouldDie_prop :: Bool
killMob_setsShouldDie_prop = killMob mob ^. shouldDie
  where
    mob = initMob (1, 1)
    
spec :: Spec
spec = do
  describe "initMob" $ do
    it "should set direction to Left" $ property
      initMob_setsDefaultDirection_prop
    it "should set shouldDie to False" $ property
      initMob_setsShouldDie_prop
  describe "killMob" $ do
    it "should set shouldDie to True" $ property
      killMob_setsShouldDie_prop
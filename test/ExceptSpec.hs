{- HLINT ignore "Use camelCase" -}
{- HLINT ignore "Redundant do" -}

module ExceptSpec where

import Control.Monad.Except
import Control.Monad.Identity
import Data.Except
import Test.Hspec
import Test.QuickCheck

assertMaybeExcept_just_prop :: String -> String -> Bool
assertMaybeExcept_just_prop value1 value2 =
  runIdentity
    ( runExceptT (assertMaybeExcept (Just value1) value2 :: ExceptT String Identity String)
    )
    == Right value1

assertMaybeExcept_nothing_prop :: String -> Bool
assertMaybeExcept_nothing_prop value =
  runIdentity
    ( runExceptT (assertMaybeExcept Nothing value :: ExceptT String Identity String)
    )
    == Left value

assertExcept_true_prop :: String -> Bool
assertExcept_true_prop value =
  runIdentity
    ( runExceptT (assertExcept True value :: ExceptT String Identity ())
    )
    == Right ()

assertExcept_false_prop :: String -> Bool
assertExcept_false_prop value =
  runIdentity
    ( runExceptT (assertExcept False value :: ExceptT String Identity ())
    )
    == Left value

spec :: Spec
spec = do
  describe "assertMaybeExcept" $ do
    describe "when the value is Just x" $ do
      it "returns Right x" $ property
        assertMaybeExcept_just_prop
    describe "when the value is Nothing" $ do
      it "returns Left with the error message" $ property
        assertMaybeExcept_nothing_prop
  describe "assertExcept" $ do
    describe "when the value is True" $ do
      it "returns Right with the accumulated value" $ property
        assertExcept_true_prop
    describe "when the value is False" $ do
      it "returns Left with the error message" $ property
        assertExcept_false_prop

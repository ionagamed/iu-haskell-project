{- HLINT ignore "Use camelCase" -}
{- HLINT ignore "Redundant do" -}

module CharacterSpec where

import Control.Lens
import Test.Hspec
import Test.QuickCheck

import Types.Character
import Types.SmoothPosition

testInit :: (Float, Float) -> Bool -> Int -> Float -> Character
testInit pos' handledThisLoop' waitBeats' damageAnimationTime'
  = set handledThisLoop handledThisLoop'
  $ set waitBeats waitBeats'
  $ set damageAnimationTime damageAnimationTime'
  $ initCharacter pos'

initCharacter_setsPosition_prop :: (Float, Float) -> Bool
initCharacter_setsPosition_prop pos =
  initCharacter pos ^. position == initSmoothPositionAt pos

initCharacter_setsHandledThisLoop_prop :: Property
initCharacter_setsHandledThisLoop_prop
  = forAll
    arbitrary
    (\pos -> not (initCharacter pos ^. handledThisLoop))

initCharacter_setsWaitBeats_prop :: Property
initCharacter_setsWaitBeats_prop
  = forAll
    arbitrary
    (\pos -> (initCharacter pos ^. waitBeats) == 1)

initCharacter_setsDamageAnimationTime_prop :: Property
initCharacter_setsDamageAnimationTime_prop
  = forAll
    arbitrary
    (\pos -> (initCharacter pos ^. damageAnimationTime) == 0)

characterCanMove_waitBeatsNotZero_prop :: Property
characterCanMove_waitBeatsNotZero_prop
  = forAll
    (arbitrary `suchThat` (/= 0))
    (\x -> not $ characterCanMove (testInit (2, 2) False x 1))

characterCanMove_waitBeatsZero_prop :: Property
characterCanMove_waitBeatsZero_prop
  = forAll
    (arbitrary `suchThat` (== 0))
    (\x -> characterCanMove (testInit (2, 2) False x 1))

moveCharacter_shouldWait :: Bool
moveCharacter_shouldWait =
  moveCharacter 0.24 1 (1, 0) oldCharacter == newCharacter
  where
    oldCharacter = testInit (1, 1) False 1 0
    newCharacter = testInit (1, 1) True 0 0

moveCharacter_movesCharacter_prop :: Bool
moveCharacter_movesCharacter_prop =
  moveCharacter 1 1 (1, 0) oldCharacter == newCharacter
  where
    oldCharacter = testInit (1, 1) False 0 0
    newCharacter 
      = set
          (position . time)
          1
        (testInit (2, 1) False 1 0) 

spec :: Spec
spec = do
  describe "initCharacter" $ do
    it "should init character on given position" $ property
      initCharacter_setsPosition_prop
    it "should init character who didn't handle this loop" $ property
      initCharacter_setsHandledThisLoop_prop
    it "should init character with wait beats = 1" $ property
      initCharacter_setsWaitBeats_prop
    it "should init character with damage animation time = 0" $ property
      initCharacter_setsDamageAnimationTime_prop
  describe "characterCanMove" $ do
    describe "when waitBeats /= 0" $
      it "should be false" $ property
        characterCanMove_waitBeatsNotZero_prop
    describe "when waitBeats = 0" $
      it "should be true" $ property
        characterCanMove_waitBeatsZero_prop
  describe "moveCharacter" $ do
    describe "when character needs to wait" $
      it "should not move character and should handle this loop" $ property
        moveCharacter_shouldWait
    describe "when character can move" $
      it "should move character" $ property
        moveCharacter_movesCharacter_prop


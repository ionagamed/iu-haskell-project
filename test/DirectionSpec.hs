{- HLINT ignore "Use camelCase" -}
{- HLINT ignore "Redundant do" -}

module DirectionSpec where

import Test.Hspec
import Test.QuickCheck

import Types.Direction
import Prelude hiding (Left, Right)

estimateDirection_right_prop :: Property
estimateDirection_right_prop
  = forAll
    (arbitrary `suchThat` \(x, _) -> x > 0)
    (\vec -> estimateDirection vec == Right)

estimateDirection_left_prop :: Property
estimateDirection_left_prop
  = forAll
    (arbitrary `suchThat` \(x, _) -> x < 0)
    (\vec -> estimateDirection vec == Left)

estimateDirection_up_prop :: Property
estimateDirection_up_prop
  = forAll
    (arbitrary `suchThat` \y -> y < 0)
    (\y -> estimateDirection (0, y) == Up)

estimateDirection_down_prop :: Property
estimateDirection_down_prop
  = forAll
    (arbitrary `suchThat` \y -> y > 0)
    (\y -> estimateDirection (0, y) == Down)

estimateDirection_none_prop :: Bool
estimateDirection_none_prop = 
  estimateDirection (0, 0) == None
  
oppositeDirection_up_prop :: Bool
oppositeDirection_up_prop =
  oppositeDirection Down == Up
  
oppositeDirection_down_prop :: Bool
oppositeDirection_down_prop =
  oppositeDirection Up == Down
  
oppositeDirection_left_prop :: Bool
oppositeDirection_left_prop =
  oppositeDirection Right == Left
  
oppositeDirection_right_prop :: Bool
oppositeDirection_right_prop =
  oppositeDirection Left == Right

spec :: Spec
spec = do
  describe "estimateDirection" $ do
    describe "when x > 0" $ do
      it "should return Right" $ property estimateDirection_right_prop
    describe "when x < 0" $ do
      it "should return Left" $ property estimateDirection_left_prop
    describe "when y < 0" $ do
      it "should return Up" $ property estimateDirection_up_prop
    describe "when y > 0" $ do
      it "should return Down" $ property estimateDirection_down_prop
    describe "when x = 0 and y = 0" $ do
      it "should return None" $ property estimateDirection_none_prop
  describe "oppositeDirection" $ do
    describe "when Down" $ do
      it "should return Up" $ property oppositeDirection_up_prop
    describe "when Up" $ do
      it "should return Down" $ property oppositeDirection_down_prop
    describe "when Left" $ do
      it "should return Right" $ property oppositeDirection_right_prop
    describe "when Right" $ do
      it "should return Left" $ property oppositeDirection_left_prop


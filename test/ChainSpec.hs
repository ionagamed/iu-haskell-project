{- HLINT ignore "Use camelCase" -}
{- HLINT ignore "Redundant do" -}

module ChainSpec where

import Test.Hspec
import Test.QuickCheck

import Data.Chain

chain_empty_prop :: Int -> Bool
chain_empty_prop value =
  chain [] value == value

chain_rightToLeft_prop :: Blind (Int -> Int) -> Blind (Int -> Int) -> Int -> Bool
chain_rightToLeft_prop (Blind f) (Blind g) value =
  chain [f, g] value == (f . g) value

chainM_empty_prop :: Int -> Bool
chainM_empty_prop value =
  -- Using list as a monad here.
  chainM [] value == [value]

chainM_rightToLeft_prop :: Blind (Int -> Maybe Int) -> Blind (Int -> Maybe Int) -> Int -> Bool
chainM_rightToLeft_prop (Blind f) (Blind g) value =
  chainM [f, g] value == (g value >>= f)

-- TODO: test that monads get executed only until the first one fails
-- chainM_state_prop :: Bool
-- chainM_state_prop =
  -- execWriterT $ chainM [f1, f2, f3] 1 
    -- where
      -- f1 x = tell 1 >> return x
      -- f2 x


spec :: Spec
spec = do
  describe "chain" $ do
    describe "when fs is empty" $ do
      it "should return input value" $ property
        chain_empty_prop
    describe "when there are two functions" $ do
      it "should apply them right-to-left" $ property
        chain_rightToLeft_prop
  describe "chainM" $ do
    describe "when fs is empty" $ do
      it "should return input value in a monad" $ property
        chainM_empty_prop
    describe "when there are two functions" $ do
      it "should apply them right-to-left, combining the monad effects" $ property
        chainM_rightToLeft_prop

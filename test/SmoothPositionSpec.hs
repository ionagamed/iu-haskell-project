{- HLINT ignore "Use camelCase" -}
{- HLINT ignore "Redundant do" -}

module SmoothPositionSpec where

import Control.Lens
import Test.Hspec
import Test.QuickCheck

import Types.SmoothPosition
import Math

testInit :: (Float, Float) -> (Float, Float) -> Float -> SmoothPosition
testInit from' to' time'
  = set time time'
  $ set current to'
  $ set previous from'
  $ initSmoothPosition

initWithTime :: (Float, Float) -> Float -> SmoothPosition
initWithTime coords time' = set time time' $ initSmoothPositionAt coords

initSmoothPositionAt_setsCurrent_prop :: (Float, Float) -> Bool
initSmoothPositionAt_setsCurrent_prop coords =
  initSmoothPositionAt coords ^. current == coords

initSmoothPositionAt_setsPrevious_prop :: (Float, Float) -> Bool
initSmoothPositionAt_setsPrevious_prop coords =
  initSmoothPositionAt coords ^. previous == coords

intermediateSmoothPosition_zeroTime_prop :: (Float, Float) -> (Float, Float) -> Bool
intermediateSmoothPosition_zeroTime_prop from' to' =
  intermediateSmoothPosition pos == to'
    where
      pos = testInit from' to' 0

intermediateSmoothPosition_fullTime_prop :: (Float, Float) -> (Float, Float) -> Bool
intermediateSmoothPosition_fullTime_prop from' to' =
  intermediateSmoothPosition pos == from'
    where
      pos = testInit from' to' 1

intermediateSmoothPosition_midTime_prop :: (Float, Float) -> (Float, Float) -> Bool
intermediateSmoothPosition_midTime_prop from' to' =
  intermediate >= mins && intermediate <= maxs
    where
      intermediate = intermediateSmoothPosition pos
      pos = testInit from' to' 0.5
      mins = (min (fst from') (fst to'), min (snd from') (snd to'))
      maxs = (max (fst from') (fst to'), max (snd from') (snd to'))

swapSmoothPosition_prop :: (Float, Float) -> (Float, Float) -> Bool
swapSmoothPosition_prop from' to' =
  pos ^. current == from' && pos ^. previous == to'
    where
      pos = swapSmoothPosition $ testInit from' to' 0

moveSmoothPosition_previous_prop :: (Float, Float) -> (Float, Float) -> Bool
moveSmoothPosition_previous_prop from' to' =
  pos ^. previous == from'
    where
      pos = moveSmoothPosition (vSub to' from') $ initSmoothPositionAt from'

moveSmoothPosition_current_prop :: (Float, Float) -> (Float, Float) -> Bool
moveSmoothPosition_current_prop from' to' =
  pos ^. current == to'
    where
      pos = moveSmoothPosition (vSub to' from') $ initSmoothPositionAt from'

moveSmoothPosition_time_prop :: (Float, Float) -> (Float, Float) -> Bool
moveSmoothPosition_time_prop from' to' =
  pos ^. time == 1
    where
      pos = moveSmoothPosition (vSub to' from') $ initSmoothPositionAt from'


spec :: Spec
spec = do
  describe "initSmoothPositionAt" $ do
    it "should set the current field to argument" $ property
      initSmoothPositionAt_setsCurrent_prop
    it "should set the previous field to argument" $ property
      initSmoothPositionAt_setsPrevious_prop
  describe "intermediateSmoothPosition" $ do
    describe "when time = 0" $ do
      xit "should return the current position" $ property
        intermediateSmoothPosition_zeroTime_prop
    describe "when time = max" $ do
      it "should return the previous position" $ property
        intermediateSmoothPosition_fullTime_prop
    describe "when time is something in between" $ do
      xit "should return something between current and previous" $ property
        intermediateSmoothPosition_midTime_prop
  describe "swapSmoothPosition" $ do
    it "should swap the current and previous values" $ property
      swapSmoothPosition_prop
  describe "moveSmoothPosition" $ do
    it "should set previous to current" $ property
      moveSmoothPosition_previous_prop
    xit "should set current to argument" $ property
      moveSmoothPosition_current_prop
    it "should set time to 1" $ property
      moveSmoothPosition_time_prop

